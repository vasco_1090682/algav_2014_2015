% Include da base de conhecimento
:-consult('base conhecimento').

% ------------------------------------------------------
% --------------- Alinea 3) Menos trocas ---------------
% ------------------------------------------------------

% Lista todos os trajetos e os respectivos n� de trocas (predicado de teste)
listarTrocas(EO,ED):-listaTrajetos(EO,ED,LT),listarTrocas(LT).
listarTrocas([]).
listarTrocas([H|T]):-length(H,C),
	                    write('Trajeto:'),write(H),nl,
			    write('Tamanho:'),write(C),nl,
			    contarTrocas(H,Trocas),
			    write('Trocas:'),write(Trocas),nl,
	                    listarTrocas(T).

% Encontra o trajeto com menos trocas de uma dada lista de trajetos
calcularMenosTrocas([X], X) :- !.
calcularMenosTrocas([X,Y|T], Trajeto):-contarTrocas(X,CX),contarTrocas(Y,CY),
    ( CX > CY -> % se o n� trocas do trajeto X for maior que o do trajeto Y
        calcularMenosTrocas([Y|T], Trajeto) % trajeto Y � o com menos trocas atualmente
    ; %sen�o
        calcularMenosTrocas([X|T], Trajeto) % trajeto X � o com menos trocas atualmente
    ).


% Dado um trajeto, conta as trocas efetuadas ao longo desse trajeto
% contarTrocas([Trajeto],Contador de trocas)
contarTrocas([],0).
contarTrocas(L,Cont):-getCabeca(L,H), % primeira esta��o do trajeto
	              estacao(H,[LinhaAtual|_]), % linha da primeira esta��o, para se ter a linha atual
		      contarTrocas(L,LinhaAtual,0,Cont),!.

contarTrocas([],_,Trocas,Trocas).
contarTrocas([H|T],LinhaAtual,Trocas,Cont):-estacao(H,Linhas),
	                    length(Linhas,C),
			    C>1, % Se for um cruzamento � poss�vel que n�o haja troca e portanto n�o faz nada
			    contarTrocas(T,LinhaAtual,Trocas,Cont).
contarTrocas([H|T],LinhaAtual,Trocas,Cont):-estacao(H,Linhas),
	                    length(Linhas,C),
			    C==1, % Verifica se esta esta��o pertence apenas a uma linha
			    not(member(LinhaAtual,Linhas)), % E se a linha dessa esta��o for diferente da linha atual, existe uma troca e muda a linha atual
			    Trocas1 is Trocas + 1,
			    getCabeca(Linhas,LinhaAtual1),
			    contarTrocas(T,LinhaAtual1,Trocas1,Cont).
contarTrocas([H|T],LinhaAtual,Trocas,Cont):-estacao(H,Linhas),
	                    length(Linhas,C),
			    C==1, % Caso a esta��o perten�a a uma linha mas essa linha � igual � esta��o atual, n�o faz nada
			    contarTrocas(T,LinhaAtual,Trocas,Cont).

% -----------------------------------------------------
% --------------- Al�nea 3) Mais r�pido ---------------
% -----------------------------------------------------

% Encontra o trajeto com menor tempo de viagem de uma dada lista
% de trajetos
calcularMaisRapido([X], X) :- !.
calcularMaisRapido([X,Y|T], Trajeto):-calcularTempo(X,TX),calcularTempo(Y,TY),
    ( TX > TY -> % se o tempo de viagem do trajeto X for maior que o do trajeto Y
        calcularMaisRapido([Y|T], Trajeto) % trajeto Y � o mais r�pido
    ; %sen�o
        calcularMaisRapido([X|T], Trajeto) % trajeto X � o mais r�pido
    ).

% Lista todos os trajetos e os respectivos tempos de viagem (predicado
% de teste)
listarTempos(EO,ED):-listaTrajetos(EO,ED,LT),listarTempos(LT).
listarTempos([]).
listarTempos([H|T]):-length(H,C),
		    write('Trajeto:'),write(H),nl,
		    write('Tamanho:'),write(C),nl,
		    calcularTempo(H,Tempo),
		    write('Tempo viagem:'),write(Tempo),nl,
	            listarTempos(T).


% Dado um trajeto, calcula o tempo total de viagem da esta��o
% inicial at� � esta��o final
% calcularTempo([Trajeto],Soma tempo)
calcularTempo([],0).
calcularTempo(L,Soma):-calcularTempo(L,0,Soma),!.
calcularTempo([],Soma,Soma). %condi��o de paragem
calcularTempo([H|T],Soma,Cont):-getCabeca(T,H1),
                                (liga(H,H1,Tempo);liga(H1,H,Tempo)),
			        Soma1 is Soma + Tempo,
			        calcularTempo(T, Soma1,Cont).
% quando o getCabeca falha porque a lista s� tem um elemento, nanda
% uma lista vazia onde terminar� na condi��o de paragem
calcularTempo(L,Soma,Cont):-length(L,C),
	                    C==1,
			    calcularTempo([],Soma,Cont).

% -------------------------------------------------------------
% --------------- Al�nea 3) Menor percurso a p� ---------------
% -------------------------------------------------------------

% Dada uma lista de trajetos encontra o trajeto com menor tempo de
% desloca��o na troca de linhas numa esta��o
calcularMenorDeslocacao([X], X) :- !.
calcularMenorDeslocacao([X,Y|T], Trajeto):-somarTempos(X,TX),somarTempos(Y,TY),
    ( TX > TY -> % se o tempo de viagem do trajeto X for maior que o do trajeto Y
        calcularMenorDeslocacao([Y|T], Trajeto) % trajeto Y � o mais r�pido
    ; %sen�o
        calcularMenorDeslocacao([X|T], Trajeto) % trajeto X � o mais r�pido
    ).



% Lista todos os trajetos e os respectivos tempos de viagem (predicado
% de teste)
listarDeslocacoes(EO,ED):-listaTrajetos(EO,ED,LT),listarDeslocacoes(LT),!.
listarDeslocacoes([]).
listarDeslocacoes([H|T]):-length(H,C),
			  write('Trajeto:'),write(H),nl,
		          write('Tamanho:'),write(C),nl,
		          encontrarLinhas(H,ListaLinhas),
			  write('Linhas:'),write(ListaLinhas),nl,
			  somarTempos(H,Tempo),
			  write('Tempo desloca��o:'),write(Tempo),nl,
	                  listarDeslocacoes(T).

% Dado um trajeto calcula o tempo de desloca�ao nas trocas entre linhas
somarTempos([],0).
somarTempos(L,Tempo):-somarTempos(L,0,Tempo,2),!. %come�a na posi��o 2
somarTempos([],Tempo,Tempo,_). %condi��o de paragem
somarTempos(L,Tempo,TempoAux,Cont):-length(L,C),
	                            Cont==C, % quando o contador for igual ao tamanho da lista ir� terminar
			            TempoAux is Tempo,
			            somarTempos([],Tempo,Tempo,Cont).
somarTempos(L,Tempo,TempoAux,Cont):-ContAux1 is Cont-1,
	                            elemIndice(L,EA,ContAux1), % vai buscar a esta��o anteriror
			            ContAux2 is Cont+1,
			            elemIndice(L,ES,ContAux2), % vai buscar a esta��o seguinte
			            estacao(EA,[Linha1|_]), % linha da esta��o anterior
			            estacao(ES,[Linha2|_]), % linha da esta��o seguinte
			            Linha1 \= Linha2, % se as linhas s�o diferentes, houve troca e ent�o vai buscar o tempo de desloca��o entre essas linhs na esta��o no index Cont
			            elemIndice(L,X,Cont), %esta��o do index Cont
			            (cruzamento(X,Linha1,Linha2,TempD);cruzamento(X,Linha2,Linha1,TempD)), % tempo dessa esta��o/linhas
			            Tempo1 is Tempo+TempD,
			            Cont1 is Cont+1, % passa para a esta��o seguinte
			            somarTempos(L,Tempo1,TempoAux,Cont1).
somarTempos(L,Tempo,TempoAux,Cont):-Cont1 is Cont+1, % se falhou no predicado anterior em Linha1 \= Linha2, apenas incrementa o contador, para passar para a esta��o seguinte
	                            somarTempos(L,Tempo,TempoAux,Cont1).


% Dado um trajeto, encontra as linhas por onde passou
% encontrarLinhas(Trajeto, Lista de linhas).
encontrarLinhas(L,ListaLinhas1):-getCabeca(L,H), % primeira esta��o do trajeto
				 estacao(H,[LinhaAtual|_]), % linha da primeira esta��o, para se ter a linha atual
	                         encontrarLinhas(L,LinhaAtual,[],ListaLinhas), % encontra todas as linhas por onde o trajeto passou excepto a linha inicial
                                 inverte(ListaLinhas,ListaLinhasInv), % inverte a lista de linhas para depois ficarem na ordem correta
				 insere(LinhaAtual,ListaLinhasInv,ListaLinhas1), % insere a linha inicial � lista de linhas
				 !.

% encontrarLinhas([Trajeto], Linha atual, Lista de linhas, lista de
% linhas auxiliar).
encontrarLinhas([],_,ListaLinhas,ListaLinhas).
encontrarLinhas([H|T],LinhaAtual,LL,LLA):-estacao(H,Linhas),
	                                  length(Linhas,C),
					  C>1, % Se for um cruzamento � poss�vel que n�o haja troca e portanto n�o faz nada
					  encontrarLinhas(T,LinhaAtual,LL,LLA).

encontrarLinhas([H|T],LinhaAtual,LL,LLA):-estacao(H,Linhas),
	                                  length(Linhas,C),
			                  C==1, % Verifica se esta esta��o pertence apenas a uma linha
					  not(member(LinhaAtual,Linhas)), % E se a linha dessa esta��o for diferente da linha atual, existe uma troca, mudando a linha atual e inserindo essa linha na lista de linhas
			                  getCabeca(Linhas,LinhaAtual1),
			                  insere(LinhaAtual1,LL,LL1),
					  encontrarLinhas(T,LinhaAtual1,LL1,LLA).
encontrarLinhas([H|T],LinhaAtual,LL,LLA):-estacao(H,Linhas),
	                                  length(Linhas,C),
			                  C==1, % Caso a esta��o perten�a a uma linha mas essa linha � igual � linha atual, n�o faz nada
			                  encontrarLinhas(T,LinhaAtual,LL,LLA).


% ------------------------------------------
% --------------- Alinea 5)  ---------------
% ------------------------------------------


% Somar tempos de visita numa lista de locais
% somarTemposVisita([Lista de locais],Soma dos tempos dos locais).
somarTemposVisita(L,TempoTotal):-somarTemposVisita(L,0,TempoTotal).
somarTemposVisita([],TempoTotal,TempoTotal).
somarTemposVisita([H|T],TempoTotal,Cont):-localTuristico(H,TempoLocal,_),
					  TempoTotal1 is TempoTotal+TempoLocal,
					  somarTemposVisita(T,TempoTotal1,Cont).


% Dada uma lista de trajetos encontra o trajeto com menor tempo de
% viagem e desloca��o menor que 5 horas ou maior que 5 horas e menor que
% 8 horas, dependendo do tipo de visita
% encontrarVisita([Lista de trajetos],Trajato escolhido, Horas, Somas
% tempos visita nos locais)
encontrarVisita(LT,Trajeto,300,Tempo):-encontrarVisita1(LT,[],Trajeto,Tempo),!.
encontrarVisita(LT,Trajeto,480,Tempo):-encontrarVisita2(LT,[],Trajeto,Tempo),!.

% Visitas meio dia
encontrarVisita1([],Trajeto,Trajeto,_) :- !.
encontrarVisita1([H|T],Trajeto,TrajetoAux,Tempo):-calcularTempo(H,TH), % calcula o tempo de viagem de um trajeto
					               TH1 is TH+Tempo, % soma esse tempo de viagem ao tempo de visita
    ( TH1 =< 300 -> % se o tempo total for menor ou igual que 300
        encontrarVisita1(T,H,TrajetoAux,Tempo) % encontrou um visita
    ; %sen�o
        encontrarVisita1(T,Trajeto,TrajetoAux,Tempo) % vai procurar nos restantes trajetos um visita v�lida
    ).

% Visitas dia inteiro
encontrarVisita2([],Trajeto,Trajeto,_) :- !.
encontrarVisita2([H|T],Trajeto,TrajetoAux,Tempo):-calcularTempo(H,TH), % calcula o tempo de viagem de um trajeto
					               TH1 is TH+Tempo, % soma esse tempo de viagem ao tempo de visita
    ( TH1 > 300, TH1 =< 480 -> % se o tempo total for maior que 300 e menor ou igual a 480
        encontrarVisita2(T,H,TrajetoAux,Tempo) % encontrou um visita
    ; %sen�o
        encontrarVisita2(T,Trajeto,TrajetoAux,Tempo) % vai procurar nos restantes trajetos um visita v�lida
    ).

% Dada uma lista de locais, remove o primeiro o ultimo e vai buscar a
% esta��o dos restantes
% estacoesRestantesLocais([Lista locais],[Lista esta��es]).
estacoesRestantesLocais(LL,LE):-cortarPrimeiroUltimo(LL,LL1), % remove o primeiro e �ltimo local
				estacoesRestantesLocais(LL1,[],LE). % obter a respetiva esta��o dos restantes locais
estacoesRestantesLocais([],LL,LL).
estacoesRestantesLocais([H|T],L,LA):-localTuristico(H,_,[E|_]),
	                             insere(E,L,L1),
				     estacoesRestantesLocais(T,L1,LA).

% Verifica se uma dada lista de esta��es est�o presentes num dado trajeto
% verificaEstacoes([Trajeto],[Esta��es]).
verificaEstacoes(_,[]):-!.
verificaEstacoes(Trajeto,[H|T]):-member(H,Trajeto),!,verificaEstacoes(Trajeto,T).

% Remover os trajetos que n�o passam numa dada lista de esta��es
%  removerTrajetos([Lista trajetos],[Lista esta��es],[Lista apenas com
%  os trajetos desejados])
removerTrajetos(LT,LE,LTF):-removerTrajetos(LT,LE,[],LTF).
removerTrajetos([],_,LTF,LTF):- !.
removerTrajetos([H|T],LE,LTF,LTFAux):-( verificaEstacoes(H,LE) -> % se o tempo total for maior que 300 e menor ou igual a 480
				          insere(H,LTF,LTF1),
                                          removerTrajetos(T,LE,LTF1,LTFAux) % encontrou um visita
				      ; %sen�o
                                          removerTrajetos(T,LE,LTF,LTFAux) % vai procurar nos restantes trajetos um visita v�lida
				      ).


% ------------------------------------------
% --------------- Alinea 6)  ---------------
% ------------------------------------------

% visitaExportar(Local de in�cio,Hora in�cio,Dia,[Locais tur�sticos a
% visitar],ficheiro)
% Local de in�cio = esta��o ou local tur�stico
visitaExportar1(Estacao,Hora,Dia,LocaisTur,Stream):-% Imprime todos os trajetos para visitar os locais e os respetivos fins tempos de visita
					 trajetosIntermedios(Estacao,LocaisTur,Hora,Dia,HoraFim,Stream),
					 %trajeto de volta para esta��o inicial
					 inverte(LocaisTur,LTInv), % inverte a lista de locais tur�sticos para ir buscar � cabeca �ltimo local visitado e a respetiva esta��o
					 elemIndice(LTInv,Local1,1), % obter esse �ltimo local tur�stico
					 localTuristico(Local1,_,[EL|_]), % obter esta��o mais pr�xima desse local
					 listaTrajetos(EL,Estacao,LT1), % encontrar todos os trajetos entre a esta��o do �ltimo local e a esta��o inicial
					 calcularMaisRapido(LT1,MR1), % encontrar o trajeto mais r�pido
					 write(Stream,'Trajeto de volta a '),write(Stream,Estacao),
					 nl(Stream),write(Stream,MR1),nl(Stream),
					 %calcular tempo de fim da visita
                                         calcularTempo(MR1,TV1), % calcular o tempo de viagem desse trajeto
					 somarHoras(HoraFim,TV1,HoraFinal), % obter o tempo final da visita
					 write(Stream,'Hora fim visita: '),write(Stream,HoraFinal),nl(Stream).


% Dada uma esta��o inicial e uma lista de locais, imprime o trajeto e o
% hora final da esta��o inicial � cabe�a da lista de locais
trajetosIntermedios(EI,L,HoraInicio,Dia,HoraFim,Stream):-trajetosIntermedios1(EI,L,HoraInicio,Dia,HoraFim,Stream),!.
trajetosIntermedios1(_,[],Hora,_,Hora,_).
trajetosIntermedios1(EI,[H|T],Hora,Dia,HoraAux,Stream):-
				     localTuristico(H,TempoVisita,[EL|_]), % obter esta��o mais pr�xima do local e o tempo de visita nesse local
				     validarHorarioLocal(H,Hora,Dia), % valida se o local tur�stico est� em funcionamento �quel hora
	                             listaTrajetos(EI,EL,LT), % encontra todos (LT) os trajetos poss�veis entre as duas esta��es
				     calcularMaisRapido(LT,MR), %encontra o trajeto com mais r�pido (MR)
				     write(Stream,'Trajeto para '),write(Stream,H),nl(Stream),write(Stream,MR),nl(Stream),
				     % calcular o tempo que demora a viagem do trajeto mais o tempo de visita
				     calcularTempo(MR,TV), % calcular o tempo do trajeto mais r�pido
				     Tempo is TV+TempoVisita, % somar o tempo de viagem mais o tempo de visita
				     somarHoras(Hora,Tempo,HoraFimVisita), % devolver a hora final
				     write(Stream,'Fim de visita a '),write(Stream,H),write(Stream,': '),
				     write(Stream,HoraFimVisita),nl(Stream),nl(Stream),
				     trajetosIntermedios1(EL,T,HoraFimVisita,Dia,HoraAux,Stream).



% -----------------------------------------------------
% --------------- Predicados Auxiliares ---------------
% -----------------------------------------------------

% Devolve a cabe�a de uma dada lista
getCabeca([H|_],H).

% Encontrar todos os trajetos dada uma esta��o origem e uma esta��o
% destino
trajeto(EO,ED,L):-trajeto(EO,ED,[EO],L).
trajeto(EO,EO,_,[EO]).
trajeto(EO,ED,LA,[EO|L]):-(liga(EO,X,_);liga(X,EO,_)),
	                  not(member(X,LA)),
			  trajeto(X,ED,[X|LA],L).

% Criar uma lista com todos os trajetos
% listaTrajetos(Esta��o origem, Esta��o destino, Lista de trajetos).
listaTrajetos(EO,ED,LT):-findall(L,trajeto(EO,ED,L),LT).

% validarHora(Hora,Dia,Esta��o). Dada uma hora determina se a esta��o
% est� em funcionamento �quela hora
validarHora(Hora,Dia,E):-estacao(E,Linhas),validarHora1(Hora,Dia,Linhas).
validarHora1(Hora,Dia,[H|_]):-linhaHorario(H,HoraI,HoraF,Dia,_),
			      Hora >= HoraI,
			      Hora =< HoraF.

% inserir elemento numa lista
insere(X,L,[X|L]).

% devolve o elemento da lista num dado index
elemIndice([Elem|_], Elem, 1). % condi��o de pagarem
elemIndice([_|T], Elem, I):-elemIndice(T, Elem, I1),
	                 I is I1+1.  % incrementa o index

% inverte os elementos de uma lista
inverte(L,Inv):-  inverte(L,[],Inv).
inverte([H|T],A,Inv):- inverte(T,[H|A],Inv).
inverte([],A,A).

% remover o primeiro e �ltimo de uma lista
cortarPrimeiroUltimo(L,L1):-cortarCabeca(L,LA1), % remove o primeiro
	                    inverte(LA1,LA2), % inverte
			    cortarCabeca(LA2,LA3), % remove o �ltimo que agora � a cabe�a
			    inverte(LA3,L1). % inverte para a ordem original
cortarCabeca([_|T],T).


% Soma os minutos ao formato de horas HHMM
somarHoras(H,M,HoraFinal):-Hora1 is M div 60, % obt�m o n� de horas dos minutos M (divis�o inteira)
	                   Hora2 is Hora1*100, % multiplica por 100 para poder somar ao 2 primeiro algarismos das horas H
			   HoraFinal1 is H+Hora2, % somar as horas
			   Minuto1 is M mod 60, % obter os minutos restantes que n�o ficaram nas horas (resto da divis�o)
			   HoraFinal is HoraFinal1+Minuto1.


% Validar hor�rio local tur�stico
validarHorarioLocal(Local,Hora,Dia):-horarioLocalTuristico(Local,HoraI,HoraF,Dia),
	                             Hora >= HoraI,
			             Hora =< HoraF.
