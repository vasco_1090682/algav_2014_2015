% Include dos predicados
:-consult('predicados').

% ------------------------------------------------------
% --------------- Alinea 3) Menos trocas ---------------
% ------------------------------------------------------

% Pergunta exemplo: menosTrocas(0700,'domingo','Madeleine','Bercy').

% menosTrocas(Hora,Dia,Esta��o origem,Esta��o destino)
menosTrocas(Hora,Dia,EO,ED):-validarHora(Hora,Dia,EO), % valida se a esta��o est� em funcionamento �quela hora naquele dia
			     listaTrajetos(EO,ED,LT), % encontra todos os trajetos (LT) poss�veis entre as duas esta��es
			     calcularMenosTrocas(LT,MT), %encontra o trajeto com menos trocas (MT)
			     write('Trajeto:'),write(MT),nl, %imprime o trajeto
			     contarTrocas(MT,C),
			     write('Trocas:'),write(C),nl. %imprime o n�mero de trocas desse trajeto


% -----------------------------------------------------
% --------------- Al�nea 3) Mais r�pido ---------------
% -----------------------------------------------------

% Pergunta exemlo: maisRapido(0700,'domingo','Bercy','Madeleine').

% maisRapido(Hora,Dia,Esta��o origem,Esta��o destino)
maisRapido(Hora,Dia,EO,ED):-validarHora(Hora,Dia,EO), % valida se a esta��o est� em funcionamento �quela hora naquele dia
			    listaTrajetos(EO,ED,LT), % encontra todos (LT) os trajetos poss�veis entre as duas esta��es
			    calcularMaisRapido(LT,MR), %encontra o trajeto com mais r�pido (MR)
			    write('Trajeto:'),write(MR),nl, %imprime o trajeto
			    calcularTempo(MR,C),
			    write('Tempo viagem:'),write(C),nl. %imprime o tempo de viagem desse trajeto


% -------------------------------------------------------------
% --------------- Al�nea 3) Menor percurso a p� ---------------
% -------------------------------------------------------------

% Pergunta exemplo: menorDeslocacao(0800,'domingo','Argentine','Bercy')

% menosTrocas(Hora,Dia,Esta��o origem,Esta��o destino)
menorDeslocacao(Hora,Dia,EO,ED):-validarHora(Hora,Dia,EO), % valida se a esta��o est� em funcionamento �quela hora naquele dia
			         listaTrajetos(EO,ED,LT), % encontra todos os trajetos (LT) poss�veis entre as duas esta��es
			         calcularMenorDeslocacao(LT,MD), %encontra o trajeto com menor tempo em desloca��es (MD)
			         write('Trajeto:'),write(MD),nl, %imprime o trajeto
			         somarTempos(MD,C),
			         write('Tempo em desloca��es:'),write(C),nl. %imprime o tempode em desloca��es desse trajeto


% ------------------------------------------
% --------------- Alinea 5)  ---------------
% ------------------------------------------

% Pergunta exemplo: visitarLocais(['Mus�e du Louvre','Mus�e d
% Orsay','Palais Bourbon'],8).

% visitarLocais([Lista locais tur�sticos],Horas).
% Horas = 5 horas (meio dia) ou 8 horas (dia inteiro)

% Quando a lista de locais tur�sticos tem apenas 1 local
visitarLocais(L,Horas):-length(L,C),
	          C==1,
		  elemIndice(L,Local1,1), % obter o 1� local tur�stico da lista
		  localTuristico(Local1,TempoVisita,[E1|_]), % obter esta��o mais pr�xima do local e o tempo de visita nesse local
		  Horas1 is Horas*60, % convertes as Horas para minutos
	          TempoVisita =< Horas1, % verificar se o tempo de visita � menor que 5 ou 8 horas
		  write('Locais: '),write(L),nl,
		  write('Trajeto: '),write([E1]),nl,
		  write('Tempo da visita: '),write(TempoVisita),
		  !.

% Quando a lista de locais tur�sticos tem apenas dois locais
visitarLocais(L,Horas):-length(L,C),
		        C==2,
		        elemIndice(L,Local1,1), % obter o 1� local tur�stico da lista
			elemIndice(L,Local2,C), % obter �ltimo local tur�stico da lista
			localTuristico(Local1,_,[E1|_]), % obter esta��o mais pr�xima do 1� local tur�stico
		        localTuristico(Local2,_,[E2|_]), % obter esta��o mais pr�xima do �ltimo local tur�stico
		        listaTrajetos(E1,E2,LT),% encontrar todos os trajetos
			somarTemposVisita(L,TempoLocais), % somar os tempos de visita dos locais
			Horas1 is Horas*60, % converter as Horas para minutos
			encontrarVisita(LT,Trajeto,Horas1,TempoLocais), % encontrar um trajeto com menos de 5 ou mais que 5 e menos 8 horas totail
			( length(Trajeto,C1), C1>0 -> % se retornar uma trajeto vazio n�o encontrou trajeto
			    write('Locais: '),write(L),nl,
		            write('Trajeto: '),write(Trajeto),nl,
                            write('Tempo visita: '),
			    calcularTempo(Trajeto,TempoTraj),
			    TempoTotal is TempoTraj+TempoLocais,
			    write(TempoTotal),nl
			; % sen�o encontrar nenhum trajeto
			    write('Locais: '),write(L),nl,
                            write('N�o foram encontrados trajetos para este tipo de visita.')
			),
			!.
% Quando a lista de locais tur�sticos tem mais de 2 locais
visitarLocais(L,Horas):-elemIndice(L,Local1,1), % obter o 1� local tur�stico da lista
		  length(L,C),
		  elemIndice(L,Local2,C), % obter �ltimo local tur�stico da lista
		  localTuristico(Local1,_,[E1|_]), % obter esta��o mais pr�xima do 1� local tur�stico
		  localTuristico(Local2,_,[E2|_]), % obter esta��o mais pr�xima do �ltimo local tur�stico
		  listaTrajetos(E1,E2,LT), % encontrar todos os trajetos
		  estacoesRestantesLocais(L,LE), % colocar numa lista as esta��es dos restantes locais turisticos
		  removerTrajetos(LT,LE,LTF), % de todos os trajetos encontrar os que t�m cada uma dessas esta��es
		  somarTemposVisita(L,TempoLocais), % somar os tempos de visita dos locais
		  Horas1 is Horas*60, % converter as Horas para minutos
		  encontrarVisita(LTF,Trajeto,Horas1,TempoLocais), % encontrar um trajeto com menos de 5 ou mais que 5 e menos 8 horas totail
		  (length(Trajeto,C1), C1>0 -> % se retornar uma trajeto vazio n�o encontrou trajeto
		      write('Locais: '),write(L),nl,
		      write('Trajeto: '),write(Trajeto),nl,
                      write('Tempo visita: '),
		      calcularTempo(Trajeto,TempoTraj),
		      TempoTotal is TempoTraj+TempoLocais,
		      write(TempoTotal),nl
		  ; % sen�o encontrar nenhum trajeto
		      write('Locais: '),write(L),nl,
                      write('N�o foram encontrados trajetos para este tipo de visita.')
		  ),
		  !.


% ------------------------------------------
% --------------- Alinea 6)  ---------------
% ------------------------------------------

% Pergunta exemplo: visitaExportar('Argentine',0800,'domingo',['Mus�e d Orsay','Palais Bourbon']). falha
% visitaExportar('Mus�e du Louvre',0900,'domingo',['Mus�e d Orsay','Tour Eiffel']). Sucesso

% visitaExportar(Local de in�cio,Hora in�cio,Dia,[Locais tur�sticos a
% visitar])
% Local de in�cio = esta��o ou local tur�stico

% Se o local de in�cio for uma esta��o
visitaExportar(Loc,Hora,Dia,LocaisTur):-estacao(Loc,_), %verifica se � uma esta��o, se falhar para para o pr�ximo predicado visitaExportar
                                    validarHora(Hora,Dia,Loc), %validar se a hora e dia introduzidos s�o v�lida para a linha da esta��o
				    open('visita.txt',write,Stream),
	                            write(Stream,'In�cio: '),write(Stream,Loc),nl(Stream),nl(Stream),
				    visitaExportar1(Loc,Hora,Dia,LocaisTur,Stream),!,close(Stream).

% Se o local de in�cio for um local tur�stico
visitaExportar(Loc,Hora,Dia,LocaisTur):-localTuristico(Loc,TempoVisita,[E|_]),
				    validarHora(Hora,Dia,E), %validar se a hora e dia introduzidos s�o v�lida para a linha da esta��o
				    validarHorarioLocal(Loc,Hora,Dia), % validar se o local tur�stico est� aberto �quela hora/dia
				    open('visita.txt',write,Stream),
				    write(Stream,'In�cio: '),write(Stream,Loc),
				    write(Stream,'/'),write(Stream,E),nl(Stream),nl(Stream),
				    % Como come�a num local tur�stico mostra a hora a que termina a visita a esse local
				    somarHoras(Hora,TempoVisita,HoraFimLoc),
				    write(Stream,'Fim de visita a '),write(Stream,Loc),
				    write(Stream,': '),write(Stream,HoraFimLoc),nl(Stream),nl(Stream),
				    visitaExportar1(E,HoraFimLoc,Dia,LocaisTur,Stream),!,close(Stream).
