%localTuristico(Nome do local, Tempo estimado da visita em minutos,
% [Esta��es mais pr�ximas]).
localTuristico('Mus�e du Louvre', 180, ['Palais Royal Mus�e du Louvre','Louvre Rivoli']).
localTuristico('Mus�e d Orsay', 120, ['Solf�rino','Assembl�e Nationale']).
localTuristico('Palais Bourbon', 60, ['Invalides','Assembl�e Nationale']).
localTuristico('Tour Eiffel', 120, ['Bir-Hakeim','�cole Militaire']).
localTuristico('Arc de triomphe', 60, ['Charles de Gaulle �toile']).
localTuristico('Cath�drale Notre Dame de Paris', 180, ['H�tel de Ville']).


% horarioLocalTuristico(Nome do local, Hora abertura, Hora encerramento,
% Dia da semana).
% Dia da semana = dia �til, s�bado ou domingo
% Mus�e du Louvre
horarioLocalTuristico('Mus�e du Louvre',0900,1800,'dia �til').
horarioLocalTuristico('Mus�e du Louvre',0900,1800,'s�bado').
horarioLocalTuristico('Mus�e du Louvre',0900,1800,'domingo').

% Mus�e d Orsay
horarioLocalTuristico('Mus�e d Orsay',0900,1800,'dia �til').
horarioLocalTuristico('Mus�e d Orsay',0900,1800,'s�bado').
horarioLocalTuristico('Mus�e d Orsay',0900,1800,'domingo').

% Palais Bourbon
horarioLocalTuristico('Palais Bourbon',0930,1500,'dia �til').
horarioLocalTuristico('Palais Bourbon',0930,1500,'s�bado').

% Tour Eiffel
horarioLocalTuristico('Tour Eiffel',0930,2300,'dia �til').
horarioLocalTuristico('Tour Eiffel',0930,2300,'s�bado').
horarioLocalTuristico('Tour Eiffel',0930,2300,'domingo').

% Arc de triomphe
horarioLocalTuristico('Arc de triomphe',1000,2300,'dia �til').
horarioLocalTuristico('Arc de triomphe',1000,2300,'s�bado').
horarioLocalTuristico('Arc de triomphe',1000,2300,'domingo').

% Cath�drale Notre Dame de Paris
horarioLocalTuristico('Cath�drale Notre Dame de Paris',0800,1845,'dia �til').
horarioLocalTuristico('Cath�drale Notre Dame de Paris',0800,1915,'s�bado').
horarioLocalTuristico('Cath�drale Notre Dame de Paris',0800,1915,'domingo').



%linha(Nome).
linha(1).
linha(6).
linha(8).
linha(12).

% linhaHorario(Nome da linha,Hora abertura, Hora encerramento, Dia da
% semana, Frequ�ncia em minutos).
% Dia da semana = dia �til, s�bado ou domingo
% linha 1
linhaHorario(1,0600,2300,'dia �til',5).
linhaHorario(1,0600,2300,'s�bado',15).
linhaHorario(1,0700,2300,'domingo',15).

% linha 6
linhaHorario(6,0600,2300,'dia �til',5).
linhaHorario(6,0600,2300,'s�bado',15).
linhaHorario(6,0700,2300,'domingo',15).

% linha 8
linhaHorario(8,0600,2300,'dia �til',5).
linhaHorario(8,0600,2300,'s�bado',15).
linhaHorario(8,0700,2300,'domingo',15).

% linha 12
linhaHorario(12,0600,2300,'dia �til',5).
linhaHorario(12,0600,2300,'s�bado',15).
linhaHorario(12,0700,2300,'domingo',15).






% cruzamento(Nome da esta��o, Linha1, Linha2, Tempo trocar de linha em
% minutos).
cruzamento('Charles de Gaulle �toile', 1, 6, 3).
cruzamento('Concorde', 1, 8, 3).
cruzamento('Concorde', 1, 12, 3).
cruzamento('Concorde', 8, 12, 6).
cruzamento('Bastille', 1, 8, 4).
cruzamento('Reuilly-Diderot', 1, 8, 2).
cruzamento('Natlon', 1, 6, 4).
cruzamento('La Motte Picquet Grenelle', 6, 8, 4).
cruzamento('Pasteur', 6, 12, 2).
cruzamento('Montparnasse Bienvenue', 6, 12, 10).
cruzamento('Daumesnil', 6, 8, 3).
cruzamento('Madeleine', 8, 12, 3).

%estacao(Nome,[Linhas]).
estacao('La D�fense', [1]).
estacao('Esplanade de La D�fense', [1]).
estacao('Pont de Neuilly', [1]).
estacao('Les Sablons', [1]).
estacao('Porte Maillot', [1]).
estacao('Argentine', [1]).
estacao('Charles de Gaulle �toile', [1,6]).
estacao('George V', [1]).
estacao('Franklin D. Roosevelt', [1]).
estacao('Champs �lys�es Clemenceau', [1]).
estacao('Concorde', [1,8,12]).
estacao('Tuileries', [1]).
estacao('Palais Royal Mus�e du Louvre', [1]).
estacao('Louvre Rivoli', [1]).
estacao('Ch�telet', [1]).
estacao('H�tel de Ville', [1]).
estacao('St-Paul', [1]).
estacao('Bastille', [1,8]).
estacao('Gare de Lyon', [1]).
estacao('Reuilly-Diderot', [1,8]).
estacao('Natlon', [1,6]).
estacao('Porte de Vincennes', [1]).
estacao('St-Mand�', [1]).
estacao('B�raut', [1]).
estacao('Ch�teau de Vencennes', [1]).
estacao('Kl�ber', [6]).
estacao('Boissi�re', [6]).
estacao('Trocad�ro', [6]).
estacao('Passy', [6]).
estacao('Bir-Hakeim', [6]).
estacao('Dupleix', [6]).
estacao('La Motte Picquet Grenelle', [6,8]).
estacao('Cambronne', [6]).
estacao('S�vres Lecourbre', [6]).
estacao('Pasteur', [6,12]).
estacao('Montparnasse Bienvenue', [6,12]).
estacao('Edgar Quinet', [6]).
estacao('Raspail', [6]).
estacao('Denfert Rochereau', [6]).
estacao('Saint-Jacques', [6]).
estacao('Glaci�re', [6]).
estacao('Corvisart', [6]).
estacao('Place d Italie', [6]).
estacao('Nationale', [6]).
estacao('Chevaleret', [6]).
estacao('Quai de la Gare', [6]).
estacao('Bercy', [6]).
estacao('Dugommier', [6]).
estacao('Daumesnil', [6,8]).
estacao('Bel-Air', [6]).
estacao('Picpus', [6]).
estacao('Balard', [8]).
estacao('Lourmel', [8]).
estacao('Boucicaut', [8]).
estacao('F�lix Faure', [8]).
estacao('Commerce', [8]).
estacao('�cole Militaire', [8]).
estacao('La Tour Maubourg', [8]).
estacao('Invalides', [8]).
estacao('Madeleine', [8,12]).
estacao('Op�ra', [8]).
estacao('Richelieu Drouot', [8]).
estacao('Grands Boulevards', [8]).
estacao('Bonne Nouvelle', [8]).
estacao('Strasbourg Saint-Denis', [8]).
estacao('R�publique', [8]).
estacao('Filles du Calvaire', [8]).
estacao('St-S�bastien Froissart', [8]).
estacao('Chemin Vert', [8]).
estacao('Ledru-Rollin', [8]).
estacao('Faidherbe Chaligny', [8]).
estacao('Montgallet', [8]).
estacao('Michel Bizot', [8]).
estacao('Porte Dor�e', [8]).
estacao('Porte de Charenton', [8]).
estacao('Libert�', [8]).
estacao('Charenton-�coles', [8]).
estacao('�cole V�t�rinaire de Maisons-Alfort', [8]).
estacao('Maisons-Alfort-Stade', [8]).
estacao('Maisons-Alfort Les Juilliottes', [8]).
estacao('Cr�teil-L �chat', [8]).
estacao('Cr�teil-Universit�', [8]).
estacao('Cr�teil-Pr�fecture', [8]).
estacao('Cr�teil Pointe du Lac', [8]).
estacao('Mairie d issy', [12]).
estacao('Corentin Celton', [12]).
estacao('Porte de Versailles', [12]).
estacao('Convention', [12]).
estacao('Vaugirard', [12]).
estacao('Volontaires', [12]).
estacao('Falgui�re', [12]).
estacao('Notre-Dame des-Champs', [12]).
estacao('Rennes', [12]).
estacao('S�vres Babylone', [12]).
estacao('Rue du Bac', [12]).
estacao('Solf�rino', [12]).
estacao('Assembl�e Nationale', [12]).
estacao('Saint-Lazare', [12]).
estacao('Trinit� d Estienne d Orves', [12]).
estacao('Notre-Dame de-Lorette', [12]).
estacao('Saint-Georges', [12]).
estacao('Pigalle', [12]).
estacao('Abbesses', [12]).
estacao('Lamarck Caulaincourt', [12]).
estacao('Jules Joffrin', [12]).
estacao('Marcadet Poissonniers', [12]).
estacao('Marx Dormoy', [12]).
estacao('Porte de la Chapelle', [12]).
estacao('Front Populaire', [12]).

%liga(Esta��o1, Esta��o2, Tempo de viagem).
%linha 1
liga('La D�fense', 'Esplanade de La D�fense', 1).
liga('Esplanade de La D�fense', 'Pont de Neuilly', 1).
liga('Pont de Neuilly', 'Les Sablons', 1).
liga('Les Sablons', 'Porte Maillot', 1).
liga('Porte Maillot', 'Argentine', 1).
liga('Argentine', 'Charles de Gaulle �toile', 1).
liga('Charles de Gaulle �toile', 'George V', 1).
liga('George V', 'Franklin D. Roosevelt', 1).
liga('Franklin D. Roosevelt', 'Champs �lys�es Clemenceau', 1).
liga('Champs �lys�es Clemenceau', 'Concorde', 1).
liga('Concorde', 'Tuileries', 1).
liga('Tuileries', 'Palais Royal Mus�e du Louvre', 1).
liga('Palais Royal Mus�e du Louvre', 'Louvre Rivoli', 1).
liga('Louvre Rivoli', 'Ch�telet', 2).
liga('Ch�telet', 'H�tel de Ville', 1).
liga('H�tel de Ville', 'St-Paul', 1).
liga('St-Paul', 'Bastille', 1).
liga('Bastille', 'Gare de Lyon', 2).
liga('Gare de Lyon', 'Reuilly-Diderot', 1).
liga('Reuilly-Diderot', 'Natlon', 1).
liga('Natlon', 'Porte de Vincennes', 1).
liga('Porte de Vincennes', 'St-Mand�', 1).
liga('St-Mand�', 'B�raut', 1).
liga('B�raut', 'Ch�teau de Vencennes', 1).

%linha 6
liga('Charles de Gaulle �toile', 'Kl�ber', 1).
liga('Kl�ber', 'Boissi�re', 1).
liga('Boissi�re', 'Trocad�ro', 1).
liga('Trocad�ro', 'Passy', 1).
liga('Passy', 'Bir-Hakeim', 1).
liga('Bir-Hakeim', 'Dupleix', 1).
liga('Dupleix', 'La Motte Picquet Grenelle', 1).
liga('La Motte Picquet Grenelle', 'Cambronne', 1).
liga('Cambronne', 'S�vres Lecourbre', 1).
liga('S�vres Lecourbre', 'Pasteur', 1).
liga('Pasteur', 'Montparnasse Bienvenue', 1).
liga('Montparnasse Bienvenue', 'Edgar Quinet', 1).
liga('Edgar Quinet', 'Raspail', 1).
liga('Raspail', 'Denfert Rochereau', 1).
liga('Denfert Rochereau', 'Saint-Jacques', 1).
liga('Saint-Jacques', 'Glaci�re', 1).
liga('Glaci�re', 'Corvisart', 1).
liga('Corvisart', 'Place d Italie', 1).
liga('Place d Italie', 'Nationale', 1).
liga('Nationale', 'Chevaleret', 1).
liga('Chevaleret', 'Quai de la Gare', 1).
liga('Quai de la Gare', 'Bercy', 1).
liga('Bercy', 'Dugommier', 1).
liga('Dugommier', 'Daumesnil', 1).
liga('Daumesnil', 'Bel-Air', 1).
liga('Bel-Air', 'Picpus', 1).
liga('Picpus', 'Natlon', 1).

%Linha 8
liga('Balard', 'Lourmel', 1).
liga('Lourmel', 'Boucicaut', 1).
liga('Boucicaut', 'F�lix Faure', 1).
liga('F�lix Faure', 'Commerce', 1).
liga('Commerce', 'La Motte Picquet Grenelle', 1).
liga('La Motte Picquet Grenelle', '�cole Militaire', 1).
liga('�cole Militaire', 'La Tour Maubourg', 1).
liga('La Tour Maubourg', 'Invalides', 1).
liga('Invalides', 'Concorde', 1).
liga('Concorde', 'Madeleine', 1).
liga('Madeleine', 'Op�ra', 2).
liga('Op�ra', 'Richelieu Drouot', 1).
liga('Richelieu Drouot', 'Grands Boulevards', 1).
liga('Grands Boulevards', 'Bonne Nouvelle', 1).
liga('Bonne Nouvelle', 'Strasbourg Saint-Denis', 1).
liga('Strasbourg Saint-Denis', 'R�publique', 2).
liga('R�publique', 'Filles du Calvaire', 1).
liga('Filles du Calvaire', 'St-S�bastien Froissart', 1).
liga('St-S�bastien Froissart', 'Chemin Vert', 1).
liga('Chemin Vert', 'Bastille', 1).
liga('Bastille', 'Ledru-Rollin', 1).
liga('Ledru-Rollin', 'Faidherbe Chaligny', 1).
liga('Faidherbe Chaligny', 'Reuilly-Diderot', 1).
liga('Reuilly-Diderot', 'Montgallet', 1).
liga('Montgallet', 'Daumesnil', 1).
liga('Daumesnil', 'Michel Bizot', 1).
liga('Michel Bizot', 'Porte Dor�e', 1).
liga('Porte Dor�e', 'Porte de Charenton', 1).
liga('Porte de Charenton', 'Libert�', 1).
liga('Libert�', 'Charenton-�coles', 1).
liga('Charenton-�coles', '�cole V�t�rinaire de Maisons-Alfort', 1).
liga('�cole V�t�rinaire de Maisons-Alfort', 'Maisons-Alfort-Stade', 1).
liga('Maisons-Alfort-Stade', 'Maisons-Alfort Les Juilliottes', 1).
liga('Maisons-Alfort Les Juilliottes', 'Cr�teil-L �chat', 1).
liga('Cr�teil-L �chat', 'Cr�teil-Universit�', 1).
liga('Cr�teil-Universit�', 'Cr�teil-Pr�fecture', 1).
liga('Cr�teil-Pr�fecture', 'Cr�teil Pointe du Lac', 1).

%linha 12
liga('Mairie d issy', 'Corentin Celton', 1).
liga('Corentin Celton', 'Porte de Versailles', 1).
liga('Porte de Versailles', 'Convention', 1).
liga('Convention', 'Vaugirard', 1).
liga('Vaugirard', 'Volontaires', 1).
liga('Volontaires', 'Pasteur', 1).
liga('Pasteur', 'Falgui�re', 1).
liga('Falgui�re', 'Montparnasse Bienvenue', 1).
liga('Montparnasse Bienvenue', 'Notre-Dame des-Champs', 1).
liga('Notre-Dame des-Champs', 'Rennes', 1).
liga('Rennes', 'S�vres Babylone', 1).
liga('S�vres Babylone', 'Rue du Bac', 1).
liga('Rue du Bac', 'Solf�rino', 1).
liga('Solf�rino', 'Assembl�e Nationale', 1).
liga('Assembl�e Nationale', 'Concorde', 1).
liga('Concorde', 'Madeleine', 1).
liga('Madeleine', 'Saint-Lazare', 2).
liga('Saint-Lazare', 'Trinit� d Estienne d Orves', 1).
liga('Trinit� d Estienne d Orves', 'Notre-Dame de-Lorette', 1).
liga('Notre-Dame de-Lorette', 'Saint-Georges', 1).
liga('Saint-Georges', 'Pigalle', 1).
liga('Pigalle', 'Abbesses', 1).
liga('Abbesses', 'Lamarck Caulaincourt', 1).
liga('Lamarck Caulaincourt', 'Jules Joffrin', 1).
liga('Jules Joffrin', 'Marcadet Poissonniers', 1).
liga('Marcadet Poissonniers', 'Marx Dormoy', 1).
liga('Marx Dormoy', 'Porte de la Chapelle', 1).
liga('Porte de la Chapelle', 'Front Populaire', 1).
